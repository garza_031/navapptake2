﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace NavAppTake2
{
    public partial class AwesomePage : ContentPage
    {
        public AwesomePage()
        {
            InitializeComponent();

            DogPic.Source = ImageSource.FromFile("IMG_4921.png");
        }
    }
}
