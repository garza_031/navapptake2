﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace NavAppTake2
{
    public partial class NextPage : ContentPage
    {
        public NextPage(string x)
        {
            InitializeComponent();

            mainLabel.Text = x;
        }
    }
}
